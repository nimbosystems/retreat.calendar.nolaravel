<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

require_once('config.php');

$arrKeys = array_keys($_GET);
$params = explode('/',$arrKeys[0]);

switch ($params[1]) {
    case 'registrations':
        $post = file_get_contents('php://input');
        $request = (json_decode($post, true));
        $request['token'] = $retreatToken;
        $result = file_get_contents($retreatRegistrationPath."?".http_build_query($request));
        echo $result;
        break;

    case 'info':
        // Create connection
        $conn = new mysqli($db_server, $db_user, $db_password, $db_schema);
        $conn->set_charset("utf8");
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $res = $conn->query("SELECT * FROM register_info WHERE id = ".$params[2]);
            if($res->num_rows){
                echo json_encode(mysqli_fetch_assoc($res));
            }else{
                echo json_encode(['id' => $params[2]]);
            }
            $res->close();
        }
        if($_SERVER['REQUEST_METHOD'] == 'PUT'){
            $res = $conn->query("SELECT * FROM register_info WHERE id = ".$params[2]);
            $post = file_get_contents('php://input');
            $request = (json_decode($post, true));

            if($res->num_rows){
                //UPDATE
                $sql = "UPDATE register_info SET ";
                foreach(array_map('prepare_values', $request) as $key=>$value) {
                    $sql .= $key . " = " . $value . ", "; 
                }
                $sql = trim($sql, ' '); // first trim last space
                $sql = trim($sql, ','); // then trim trailing and prefixing commas
                $sql .= " WHERE id = ".$params[2];
            }else{
                //INSERT
                $keys = (implode(',',array_keys($request))); 
                $values = implode(',', array_map('prepare_values', $request));
                $sql = "INSERT INTO register_info ($keys) VALUES ($values)";
            }
            if ($conn->query($sql) === TRUE) {
                echo "Query executed successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            $res->close();
            $res = $conn->query("SELECT * FROM register_info WHERE id = ".$params[2]);
            echo json_encode(mysqli_fetch_assoc($res));
        }
        $conn->close();
        break;
}

function prepare_values($val) { //Prepare values for SQL query
    if(is_string($val))
    return sprintf("'%s'", $val);
    elseif($val === FALSE)
    return 0;
    elseif($val === TRUE)
    return 1;
    elseif(!$val)
    return 'null';
    else 
    return $val;
}