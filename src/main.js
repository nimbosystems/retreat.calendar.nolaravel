import Vue from 'vue';
import App from './App.vue';

window._ = require('lodash');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = '/api/index.php?q';


new Vue({
  el: '#app',
  render: h => h(App),
});