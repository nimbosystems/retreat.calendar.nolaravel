## Retreat Availability Calendar 
### Tests

##### Backend
- unit test if register_info requires the id field
- unit test if boolean fields are well coded (Example String "0" must be false)
- test if data can be stored locally
- test if data can be retrived from local storage
- test if it can communicate with the API
- test if the API requires a token
- test if room parameter, filter correctly
- test if start_stay and end_stay parameters, filters correctly


##### Frontend
- test if there are no registrations for the month, no days are highlighted
- test if all ocuppied days are highlighted
- test if on switch on, only the "pending" registers was displayed
- test if month change function, generate the correct month days
- test if only the "participant" guest_type are displayed
- test if all the participants in the day are displayed (confirm if it is the right behavior)
- test if on month change  the getRegistrations function is fired 
- test if on month change the getRegistrations function is fired whith the right date params
- test if availableDays display the correct amount
- test if RegisterEditor component show the right register Info
- test if multiple day reservations are highlighted
- test if multiple day reservations, all the ocuppied days was highlighted